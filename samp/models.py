from django.db import models

class Student(models.Model):
    name = models.CharField(max_length=200)
    age = models.IntegerField()
    grade = models.IntegerField()

class Attendance(models.Model):
    student_id = models.IntegerField()
    date = models.DateField(auto_now_add=True, blank=True)
    time = models.TimeField(auto_now_add=True, blank=True)

class Points(models.Model):
    student_id = models.IntegerField()
    points = models.IntegerField()

class Behaviour(models.Model):
    behaviour = models.CharField(max_length=200)
    points = models.IntegerField()
    

