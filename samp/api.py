from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie.serializers import Serializer
from samp.models import Student,Attendance,Points,Behaviour

class StudentResource(ModelResource):
    class Meta:
	queryset = Student.objects.all()
	resource_name = 'student'
	serializer = Serializer(formats=['json'])
	authorization = Authorization()
	allowed_methods = ['get','post','put']
	always_return_data = True

    def hydrate(self, bundle):
	if bundle.request.method in ['POST', ]:
	    pass
	return bundle

class AttendanceResource(ModelResource):
    class Meta:
	queryset = Attendance.objects.all()
	resource_name = 'attend'
	authorization = Authorization()
	allowed_methods = ['get','post','put']

    def hydrate(self, bundle):
	if bundle.request.method in ['POST', ]:
	    # Fetch data
	    # Assign points to bundle		
	    if Student.objects.get(id=bundle.data['student_id']):
		return bundle
	    else:
		raise Exception("Student does not exist")
	return bundle

class PointsResource(ModelResource):
    class Meta:
	queryset = Points.objects.all()
	resource_name = 'points'
	authorization = Authorization()
	allowed_methods = ['get','post','put']

    def hydrate(self, bundle):
	if bundle.request.method in ['PUT', ]:
	    beh = Behaviour.objects.get(behaviour = bundle.data['behaviour'])
	    if beh:
		points=beh.points
		student = Student.objects.get(id = bundle.data['student_id'])
		if student:
		    oldpoints = Points.objects.get(id = bundle.data['student_id']).points
		    bundle.data['points'] = points + oldpoints
		    return bundle
		else:
		    raise Exception("Student does not exist!")
	    else:
		raise Exception("Behaviour does not exist")
	return bundle
