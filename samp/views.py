from models import Student
from models import Attendance
from models import Points
from models import Behaviour
from django.http import HttpResponse

def updateattendance(request, s_id):
    """
    Check if a student exists with such id.
    """
    if Student.objects.get(id=s_id):
	entry = Attendance(student_id=s_id)
	entry.save()
	return HttpResponse("Attendance updated!")
    return HttpResponse("Invalid student id!")

def createstudent(request, s_name, s_age, s_grade):
    try:
	student = Student(name=s_name,age=s_age,grade=s_grade)
	student.save()
	return HttpResponse("Student Created!")
    except:
	return HttpResponse("Student could not be Created!")

def updatepoints(request, s_id, s_behaviour):
    beh = Behaviour.objects.get(behaviour=s_behaviour)
    if beh:
	points=beh.points
	student = Student.objects.get(student_id=s_id)
	if student:
	    student.points+=points
	    student.save()
	    return HttpResponse("Points updated!")
	return HttpResponse("Invlaid Student Id!")
    return HttpResponse("Invlaid Behaviour!")
    
