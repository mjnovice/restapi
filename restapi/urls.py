from django.conf.urls import patterns, include, url
from samp.views import updateattendance,createstudent,updatepoints
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from tastypie.api import Api
from samp.api import StudentResource, PointsResource, AttendanceResource


v1_api = Api(api_name='v1')
v1_api.register(StudentResource())
v1_api.register(PointsResource())
v1_api.register(AttendanceResource())

urlpatterns = patterns('',
    # The normal jazz here...
    (r'^api/', include(v1_api.urls)),
)
"""
urlpatterns = patterns('',
    url(r'^attend/(?P<s_id>\w+)/$', updateattendance),
    url(r'^create/(?P<s_name>\w+)/(?P<s_age>\w+)/(?P<s_grade>\w+)/$', createstudent),
    url(r'^updatepoints/(?P<s_id>\w+)/(?P<s_behaviour>\w+)/$', updatepoints),
    # Examples:
    # url(r'^$', 'restapi.views.home', name='home'),
    # url(r'^restapi/', include('restapi.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
"""